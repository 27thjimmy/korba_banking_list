<div>
    {{-- Stop trying to control. --}}

    <div class="my-2 overflow-x-auto sm:-mx-6 lg:-mx-8">
        <div class="py-2 align-middle inline-block min-w-full sm:px-6 lg:px-8">
            <div class="shadow overflow-hidden border-b border-silver-300 sm:rounded-lg">
                {{--                <button--}}
                {{--                    class="flex items-center ml-6 mb-1 bg-blue-400 hover:bg-blue-500 w-20 text-white-50 text-lg rounded-md justify-center">--}}
                {{--                    Add--}}
                {{--                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">--}}
                {{--                        <path fill-rule="evenodd"--}}
                {{--                              d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z"--}}
                {{--                              clip-rule="evenodd"/>--}}
                {{--                    </svg>--}}
                {{--                </button>--}}
                <table class="min-w-full divide-y divide-silver-200" id="table">
                    <thead class="bg-silver-50 hover:bg-silver-100">
                    <tr>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-silver-500 uppercase tracking-wider">
                            Routing Code / ID
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-silver-500 uppercase tracking-wider">
                            Participant
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-silver-500 uppercase tracking-wider">
                            Classification
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-silver-500 uppercase tracking-wider">
                            Notes
                        </th>
                        <th scope="col"
                            class="px-6 py-3 text-left text-xs font-medium text-silver-500 uppercase tracking-wider">
                            Actions
                        </th>
                    </tr>
                    </thead>
                    <tbody class="bg-white-50 divide-y divide-silver-200">
                    @if(!empty($participants))
                        @foreach($participants as $participant)
                            <tr class="hover:bg-teal-50">
                                <td class="px-6 py-4 whitespace-nowrap">
                                    {{ $participant->routing_code }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    {{ $participant->participant }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    {{ $participant->classification }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    {{ $participant->notes }}
                                </td>
                                <td class="px-6 py-4 whitespace-nowrap">
                                    <a href="{{url('edit-participant', [$participant->id])}}">
                                        <button
{{--                                            wire:click="update({{$participant->id}})"--}}
                                            class="text-key-lime-pie-800">
                                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20"
                                                 fill="currentColor">
                                                <path
                                                    d="M17.414 2.586a2 2 0 00-2.828 0L7 10.172V13h2.828l7.586-7.586a2 2 0 000-2.828z"/>
                                                <path fill-rule="evenodd"
                                                      d="M2 6a2 2 0 012-2h4a1 1 0 010 2H4v10h10v-4a1 1 0 112 0v4a2 2 0 01-2 2H4a2 2 0 01-2-2V6z"
                                                      clip-rule="evenodd"/>
                                            </svg>
                                        </button>
                                    </a>
                                    <button
                                        wire:click="destroy({{$participant->id}})"
                                        class="text-korba_red-default"
                                        onclick="return confirm('Confirm action');">
                                        <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20"
                                             fill="currentColor">
                                            <path fill-rule="evenodd"
                                                  d="M9 2a1 1 0 00-.894.553L7.382 4H4a1 1 0 000 2v10a2 2 0 002 2h8a2 2 0 002-2V6a1 1 0 100-2h-3.382l-.724-1.447A1 1 0 0011 2H9zM7 8a1 1 0 012 0v6a1 1 0 11-2 0V8zm5-1a1 1 0 00-1 1v6a1 1 0 102 0V8a1 1 0 00-1-1z"
                                                  clip-rule="evenodd"/>
                                        </svg>
                                    </button>
                                </td>
                            </tr>
                    </tbody>
                    @endforeach
                    @endif
                </table>
            </div>
        </div>
    </div>

    <!-- Add New Participant -->
    <div class="add-participant-form flex">
        <div class="m-auto space-y-6">
            @csrf
            <h2 class="text-lg">Add A New Participant</h2>

            @if(isset($successMessage))
                <div class="alert bg-green-800 rounded-md text-white-50 text-sm text-center p-2">
                    {{ $successMessage }}
                </div>
            @endif
            @if($errors->any())
                <div class="alert bg-red-400 rounded-md text-white-50 text-sm text-center p-2">
                    <ul>
                        @foreach($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <span class="text-center text-sm"></span>
            <div class="flex justify-between items-center space-x-6">
                <div class="form-group">
                    <input class="w-50 h-10 border-pigment-indigo-300 rounded-md" type="number"
                           name="routing_code"
                           wire:model="routing_code"
                           placeholder="Routing Code">
                </div>

                <div class="form-group">
                    <input class="w-50 h-10 border-pigment-indigo-300 rounded-md" type="text"
                           name="participant"
                           wire:model="participant"
                           placeholder="Participant">
                </div>
            </div>
            <div class="flex justify-between">
                <div class="form-group">
                    <input class="w-50 h-10 border-pigment-indigo-300 rounded-md" type="text"
                           name="classification"
                           wire:model="classification"
                           placeholder="Classification">
                </div>
                <div class="form-group">
                    <input class="w-50 h-10 border-pigment-indigo-300 rounded-md" type="text"
                           name="notes"
                           wire:model="notes"
                           placeholder="Notes">
                </div>
            </div>
            <button wire:click="submit()"
                    class="flex items-center ml-2 bg-blue-400 hover:bg-blue-500 w-40 h-12 text-white-50 text-lg rounded-md justify-center">
                Add
                <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                    <path fill-rule="evenodd"
                          d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z"
                          clip-rule="evenodd"/>
                </svg>
            </button>
        </div>
    </div>
</div>

<!-- Update Participant -->
{{--<div class="add-participant-form flex">--}}
{{--    <div class="m-auto space-y-6">--}}
{{--        @csrf--}}
{{--        <h2 class="text-lg">{{ $participant->participant }}: Update Details</h2>--}}

{{--        @if(isset($successMessage))--}}
{{--            <div class="alert bg-green-800 rounded-md text-white-50 text-sm text-center p-2">--}}
{{--                {{ $successMessage }}--}}
{{--            </div>--}}
{{--        @endif--}}
{{--        @if($errors->any())--}}
{{--            <div class="alert bg-red-400 rounded-md text-white-50 text-sm text-center p-2">--}}
{{--                <ul>--}}
{{--                    @foreach($errors->all() as $error)--}}
{{--                        <li>{{ $error }}</li>--}}
{{--                    @endforeach--}}
{{--                </ul>--}}
{{--            </div>--}}
{{--        @endif--}}
{{--        <input type="hidden" wire:model="selected_id">--}}
{{--        <div class="flex justify-between items-center space-x-6">--}}
{{--            <div class="form-group">--}}
{{--                <input class="w-50 h-10 border-pigment-indigo-300 rounded-md" type="number"--}}
{{--                       name="routing_code"--}}
{{--                       wire:model="routing_code"--}}
{{--                       placeholder="Routing Code"--}}
{{--                >--}}
{{--            </div>--}}

{{--            <div class="form-group">--}}
{{--                <input class="w-50 h-10 border-pigment-indigo-300 rounded-md" type="text"--}}
{{--                       name="participant"--}}
{{--                       wire:model="participant"--}}
{{--                       placeholder="Participant">--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <div class="flex justify-between">--}}
{{--            <div class="form-group">--}}
{{--                <input class="w-50 h-10 border-pigment-indigo-300 rounded-md" type="text"--}}
{{--                       name="classification"--}}
{{--                       wire:model="classification"--}}
{{--                       placeholder="Classification">--}}
{{--            </div>--}}
{{--            <div class="form-group">--}}
{{--                <input class="w-50 h-10 border-pigment-indigo-300 rounded-md" type="text"--}}
{{--                       name="notes"--}}
{{--                       wire:model="notes"--}}
{{--                       placeholder="Notes">--}}
{{--            </div>--}}
{{--        </div>--}}
{{--        <button wire:click="submit()"--}}
{{--                class="flex items-center ml-2 bg-blue-400 hover:bg-blue-500 w-40 h-12 text-white-50 text-lg rounded-md justify-center">--}}
{{--            Add--}}
{{--            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">--}}
{{--                <path fill-rule="evenodd"--}}
{{--                      d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z"--}}
{{--                      clip-rule="evenodd"/>--}}
{{--            </svg>--}}
{{--        </button>--}}
{{--    </div>--}}
{{--</div>--}}
{{--</div>--}}

