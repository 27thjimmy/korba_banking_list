<div>
{{-- If you look to others for fulfillment, you will never truly be fulfilled. --}}
<!-- Profile Information -->
    <div class="min-h-60  p-6 flex items-center justify-center">
        <div class="container max-w-screen-lg mx-auto">
            <div class="bg-white-50 rounded-md">
                <div class="bg-white rounded shadow-lg p-4 px-4 md:p-8 mb-6">
                    <div class="grid gap-4 gap-y-2 text-sm grid-cols-1 lg:grid-cols-3">
                        <div class="text-gray-600">
                            <p class="font-medium text-lg shadow-sm">Personal Details</p>
                            <p class="text-blue-600">{{ $successMessage }}</p>
                            @if($errors->any())
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li class="text-red-600">{{ $error }}
                                @endforeach
                            @endif
                        </div>

                        <div class="lg:col-span-2">
                            <div class="grid gap-4 gap-y-2 text-sm grid-cols-1 md:grid-cols-5">
                                <div class="md:col-span-5">
                                    <label for="full_name">Name</label>
                                    <input type="text" name="full_name" wire:model="full_name" id="full_name"
                                           class="h-10 border mt-1 rounded px-4 w-full bg-gray-50"
                                           value="{{ $full_name  }}"/>
                                </div>

                                <div class="md:col-span-5">
                                    <label for="email">Email Address</label>
                                    <input type="text" name="email" wire:model="email" id="email"
                                           class="h-10 border mt-1 rounded px-4 w-full bg-gray-50" value="{{ $email }}"
                                           placeholder="email@domain.com"/>
                                </div>

                                <div class="md:col-span-5 text-right">
                                    <div class="inline-flex items-end">
                                        <button wire:click="updateProfile()"
                                                class="bg-blue-600 hover:bg-blue-700 text-white-50 font-bold py-2 px-4 rounded">
                                            Submit
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Profile Information -->
</div>
