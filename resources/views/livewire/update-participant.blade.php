<div>
    {{-- Care about people's approval and you will be their prisoner. --}}
    <!-- Update Participant -->
        <div class="add-participant-form flex">
            <div class="m-auto space-y-6">
                @csrf
                <h2 class="text-lg">{{ $participant->participant }}: Update Details</h2>

                @if(isset($successMessage))
                    <div class="alert bg-green-800 rounded-md text-white-50 text-sm text-center p-2">
                        {{ $successMessage }}
                    </div>
                @endif
                @if($errors->any())
                    <div class="alert bg-red-400 rounded-md text-white-50 text-sm text-center p-2">
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <input type="hidden" wire:model="selected_id">
                <div class="flex justify-between items-center space-x-6">
                    <div class="form-group">
                        <input class="w-50 h-10 border-pigment-indigo-300 rounded-md" type="number"
                               name="routing_code"
                               wire:model="routing_code"
                               placeholder="Routing Code"
                        >
                    </div>

                    <div class="form-group">
                        <input class="w-50 h-10 border-pigment-indigo-300 rounded-md" type="text"
                               name="participant"
                               wire:model="participant"
                               placeholder="Participant">
                    </div>
                </div>
                <div class="flex justify-between">
                    <div class="form-group">
                        <input class="w-50 h-10 border-pigment-indigo-300 rounded-md" type="text"
                               name="classification"
                               wire:model="classification"
                               placeholder="Classification">
                    </div>
                    <div class="form-group">
                        <input class="w-50 h-10 border-pigment-indigo-300 rounded-md" type="text"
                               name="notes"
                               wire:model="notes"
                               placeholder="Notes">
                    </div>
                </div>
                <button wire:click="submit()"
                        class="flex items-center ml-2 bg-blue-400 hover:bg-blue-500 w-40 h-12 text-white-50 text-lg rounded-md justify-center">
                    Add
                    <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20" fill="currentColor">
                        <path fill-rule="evenodd"
                              d="M10 5a1 1 0 011 1v3h3a1 1 0 110 2h-3v3a1 1 0 11-2 0v-3H6a1 1 0 110-2h3V6a1 1 0 011-1z"
                              clip-rule="evenodd"/>
                    </svg>
                </button>
            </div>
        </div>
</div>
