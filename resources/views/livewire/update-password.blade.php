<div>
{{-- If you look to others for fulfillment, you will never truly be fulfilled. --}}
<!-- Password Information -->
    <div class="min-h-60  p-6 bg-gray-100 flex items-center justify-center">
        <div class="container max-w-screen-lg mx-auto ">
            <div class="bg-white-50 rounded-md">
                <div class="bg-white rounded shadow-lg p-4 px-4 md:p-8 mb-6">
                    <div class="grid gap-4 gap-y-2 text-sm grid-cols-1 lg:grid-cols-3">
                        <div class="text-gray-600">
                            <p class="font-medium text-lg shadow-sm">Update Password</p>
                            <p class="text-green-600">{{ $success_msg }}</p>
                            <p class="text-red-600">{{ $error_msg }}</p>
                            @if($errors->any())
                                <ul>
                                    @foreach($errors->all() as $error)
                                        <li class="text-red-600">{{ $error }}
                                @endforeach
                            @endif
                        </div>

                        <div class="lg:col-span-2">
                            <div class="grid gap-4 gap-y-2 text-sm grid-cols-1 md:grid-cols-5">
                                <div class="md:col-span-5">
                                    <label for="full_name">Current Password</label>
                                    <input type="password" name="current_pass" wire:model="current_pass" id="current_pass"
                                           class="h-10 border mt-1 rounded px-4 w-full bg-gray-50"/>
                                </div>

                                <div class="md:col-span-5">
                                    <label for="email">New Password</label>
                                    <input type="password" name="new_pass" wire:model="new_pass" id="new_pass"
                                           class="h-10 border mt-1 rounded px-4 w-full bg-gray-50"/>
                                </div>

                                <div class="md:col-span-5">
                                    <label for="email">Confirm Password</label>
                                    <input type="password" name="confirm_pass" wire:model="confirm_pass" id="confirm_pass"
                                           class="h-10 border mt-1 rounded px-4 w-full bg-gray-50"/>
                                </div>

                                <div class="md:col-span-5 text-right">
                                    <div class="inline-flex items-end">
                                        <button
                                            wire:click="updatePassword()"
                                            class="bg-blue-600 hover:bg-blue-700 text-white-50 font-bold py-2 px-4 rounded">
                                            Submit
                                        </button>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- End of Profile Information -->
</div>
