@extends('layouts.app')
@section('title', 'Profile')
@section('main')
    <div class="bg-silver-200 grid md:grid-cols-2 gap-4 h-screen w-full">
        <livewire:update-profile/>
        <livewire:update-password/>
    </div>
@endsection
