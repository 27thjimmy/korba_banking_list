<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <style>
        body {
            /*background-image: url("https://s3-us-west-2.amazonaws.com/korba-whitelabel/assets/plugins/img/korba.jpg");*/
            background-image: url("images/singapore-bank-of-china-cbd-business.jpg");
            background-size: cover;
            background-repeat: no-repeat;
        }

    </style>
</head>
<body>
<div class="container w-full h-screen flex ">
    <div class="login-form m-auto w-96 h-96 lg:w-2/5 md:h-96 space-y-1 bg-white-50 shadow-xl rounded-sm">
        <div class="login-head w-full flex justify-center">
            <img src="images/korba.png"
                 alt="Korba">
        </div>
        <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="login-main w-full p-6 flex justify-center">
                <div class="space-y-3">
                    <div class="form-group flex items-center space-x-3 w-3/4 pl-4 md:w-full md:pl-0">
                        <div class="user-icon text-silver-400 hover:text-silver-600">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20"
                                 fill="currentColor">
                                <path fill-rule="evenodd" d="M10 9a3 3 0 100-6 3 3 0 000 6zm-7 9a7 7 0 1114 0H3z"
                                      clip-rule="evenodd"/>
                            </svg>
                        </div>
                        <input
                            type="email"
                            class="appearance-none bg-transparent border-b-2 border-t-0 border-r-0 border-l-0 border-silver-300 w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none focus:ring-blue-200 @error('email') is-invalid @enderror"
                            name="email" :value="old('email')" placeholder="Email">
                    </div>

                    <div class="form-group flex items-center space-x-3 w-3/4 pl-4 md:w-full md:pl-0">
                        <div class="password-icon text-silver-400 hover:text-silver-600">
                            <svg xmlns="http://www.w3.org/2000/svg" class="h-5 w-5" viewBox="0 0 20 20"
                                 fill="currentColor">
                                <path fill-rule="evenodd"
                                      d="M5 9V7a5 5 0 0110 0v2a2 2 0 012 2v5a2 2 0 01-2 2H5a2 2 0 01-2-2v-5a2 2 0 012-2zm8-2v2H7V7a3 3 0 016 0z"
                                      clip-rule="evenodd"/>
                            </svg>
                        </div>
                        <input type="password"
                               class="appearance-none bg-transparent border-b-2 border-t-0 border-r-0 border-l-0 border-silver-300 w-full text-gray-700 mr-3 py-1 px-2 leading-tight focus:outline-none focus:ring-blue-200 @error('password') is-invalid @enderror"
                               name="password" placeholder="Password">
                    </div>
                    <div class="form-group pl-4 space-x-2">
                        <input type="checkbox"
                               class="border-silver-300 rounded text-korba_red-600 focus:ring-korba_red-500"
                               name="remember" id="remember_me">
                        <label class="form-check-label text-sm" for="remember">Remember me</label>
                    </div>
                    @error('email')
                    <div class="text-center text-red-600 text-xs">{{ $message }}</div>
                    @enderror
                    <br>
                    @if (Route::has('password.request'))
                        <a class="underline text-sm text-gray-600 hover:text-gray-900"
                           href="{{ route('password.request') }}">
                            {{ __('Forgot your password?') }}
                        </a>
                    @endif
                    <br>
                    <button
                        class="flex-shrink-0 border-transparent hover:bg-korba_red-800 text-md text-white-50 py-1 px-2 rounded-sm bg-korba_red-default w-80 h-9 justify-center shadow-sm text-sm font-medium">
                        Sign in
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
