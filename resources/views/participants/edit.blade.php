@extends('layouts.app')
@section('title', 'Update '.$participant->participant.'')
@section('main')
    <!-- Update Participant -->
    <div class="add-participant-form flex">
        <form class="m-auto space-y-6" method="post" action="{{ url('participants', [$participant->id]) }}">
            @method('PUT')
            @csrf
            <h2 class="text-xl">Update Details: {{ $participant->participant }}</h2>

            @if (session('success'))
                <h3 class="text-center text-success">{{ session('success') }}</h3>
            @endif
            @if ($errors->any())
                <div class="alert bg-red-400 rounded-md text-white-50 text-sm text-center p-2">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="flex justify-between items-center space-x-6">
                <div class="form-group">
                    <input class="w-80 h-10 border-pigment-indigo-300 rounded-md" type="number"
                           name="routing_code"
                           wire:model="routing_code"
                           value="{{ $participant->routing_code }}"
                    >
                </div>

                <div class="form-group">
                    <input class="w-80 h-10 border-pigment-indigo-300 rounded-md" type="text"
                           name="participant"
                           wire:model="participant"
                           value="{{ $participant->participant }}">
                </div>
            </div>
            <div class="flex justify-between">
                <div class="form-group">
                    <input class="w-80 h-10 border-pigment-indigo-300 rounded-md" type="text"
                           name="classification"
                           wire:model="classification"
                           value="{{ $participant->classification }}">
                </div>
                <div class="form-group">
                    <input class="w-80 h-10 border-pigment-indigo-300 rounded-md" type="text"
                           name="notes"
                           wire:model="notes"
                           value="{{ $participant->notes }}">
                </div>
            </div>
            <button
                    class="flex items-center ml-2 bg-yellow-800 hover:bg-khaki-900 w-40 h-12 text-white-50 text-lg rounded-md justify-center">
                Update
            </button>
        </form>
    </div>
@endsection
