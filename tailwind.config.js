const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
    mode: 'jit',
    purge: [
        './vendor/laravel/framework/src/Illuminate/Pagination/resources/views/*.blade.php',
        './vendor/laravel/jetstream/**/*.blade.php',
        './storage/framework/views/*.php',
        './resources/views/**/*.blade.php',
    ],

    theme: {
        colors: {
            'khaki': {
                '50': '#fefef9',
                '100': '#fefdf4',
                '200': '#fbf9e2',
                '300': '#f9f5d1',
                '400': '#f5eeaf',
                '500': '#F0E68C',
                '600': '#d8cf7e',
                '700': '#b4ad69',
                '800': '#908a54',
                '900': '#767145'
            },'yellow': {
                '50': '#fffff2',
                '100': '#ffffe6',
                '200': '#ffffbf',
                '300': '#ffff99',
                '400': '#ffff4d',
                '500': '#FFFF00',
                '600': '#e6e600',
                '700': '#bfbf00',
                '800': '#999900',
                '900': '#7d7d00'
            },'key-lime-pie': {
                '50': '#fcfdf4',
                '100': '#fafae9',
                '200': '#f2f3c9',
                '300': '#ebeca8',
                '400': '#dbdd67',
                '500': '#cccf26',
                '600': '#b8ba22',
                '700': '#999b1d',
                '800': '#7a7c17',
                '900': '#646513'
            },'pigment-indigo': {
                '50': '#f6f2f9',
                '100': '#ede6f3',
                '200': '#d2bfe0',
                '300': '#b799cd',
                '400': '#814da8',
                '500': '#4B0082',
                '600': '#440075',
                '700': '#380062',
                '800': '#2d004e',
                '900': '#250040'
            },'blue': {
                '50': '#f2f2ff',
                '100': '#e6e6ff',
                '200': '#bfbfff',
                '300': '#9999ff',
                '400': '#4d4dff',
                '500': '#0000FF',
                '600': '#0000e6',
                '700': '#0000bf',
                '800': '#000099',
                '900': '#00007d'
            },'cyan-aqua': {
                '50': '#f2ffff',
                '100': '#e6ffff',
                '200': '#bfffff',
                '300': '#99ffff',
                '400': '#4dffff',
                '500': '#00FFFF',
                '600': '#00e6e6',
                '700': '#00bfbf',
                '800': '#009999',
                '900': '#007d7d'
            },'silver': {
                '50': '#fcfcfc',
                '100': '#f9f9f9',
                '200': '#efefef',
                '300': '#e6e6e6',
                '400': '#d3d3d3',
                '500': '#C0C0C0',
                '600': '#adadad',
                '700': '#909090',
                '800': '#737373',
                '900': '#5e5e5e'
            },'teal': {
                '50': '#f2f9f9',
                '100': '#e6f2f2',
                '200': '#bfdfdf',
                '300': '#99cccc',
                '400': '#4da6a6',
                '500': '#008080',
                '600': '#007373',
                '700': '#006060',
                '800': '#004d4d',
                '900': '#003f3f'
            },'white': {
                '50': '#ffffff',
                '100': '#ffffff',
                '200': '#ffffff',
                '300': '#ffffff',
                '400': '#ffffff',
                '500': '#ffffff',
                '600': '#e6e6e6',
                '700': '#bfbfbf',
                '800': '#999999',
                '900': '#7d7d7d'
            },
            'red': {
                '50': '#fff3f3',
                '100': '#fee6e6',
                '200': '#fec1c1',
                '300': '#fd9b9b',
                '400': '#fb5151',
                '500': '#f90606',
                '600': '#e00505',
                '700': '#bb0505',
                '800': '#950404',
                '900': '#7a0303'
            },
            'korba_red': {
                'default': '#ED1944',
                '800': '#d5173d'
            },
            'green': {
                '50': '#f2fff2',
                '100': '#e6ffe6',
                '200': '#bfffbf',
                '300': '#99ff99',
                '400': '#4dff4d',
                '500': '#00ff00',
                '600': '#00e600',
                '700': '#00bf00',
                '800': '#009900',
                '900': '#007d00'
            }
        },
        extend: {
            fontFamily: {
                sans: ['Nunito', ...defaultTheme.fontFamily.sans],
            },
        },
    },

    plugins: [require('@tailwindcss/forms'), require('@tailwindcss/typography')],
};
