<?php

namespace App\Http\Livewire;

use App\Actions\Fortify\PasswordValidationRules;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Livewire\Component;

class UpdatePassword extends Component
{
    use PasswordValidationRules;

    public $current_pass;
    public $new_pass;
    public $confirm_pass;
    public $error_msg = null;
    public $success_msg = null;

    public function updatePassword()
    {
        $user = User::find(Auth::user()->id);

        $data = $this->validate([
            'current_pass' => ['required', 'string'],
//            'new_pass' => $this->passwordRules(),
        ]);
        if (! isset($this->current_pass) || ! Hash::check($this->current_pass, $user->password)) {
            $this->error_msg = 'Incorrect Password Entered';
        } elseif($this->confirm_pass != $this->new_pass) {
            $this->error_msg = 'Passwords do not match!';
        } else {
            $user->password = Hash::make($this->new_pass);
            $user->save();
            $this->success_msg = 'Your Password has been changed';
        }
    }

    public function render()
    {
        return view('livewire.update-password');
    }
}
