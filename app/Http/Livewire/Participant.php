<?php

namespace App\Http\Livewire;

use Livewire\Component;

class Participant extends Component
{
    public $participants;
    public $routing_code;
    public $participant;
    public $classification;
    public $notes;

    public $successMessage = null;

    public function mount()
    {
        $this->participants = \App\Models\Participant::all();
    }

    public function resetInput()
    {
        $this->routing_code = null;
        $this->participant = null;
        $this->classification = null;
        $this->notes = null;
    }

    public function submit()
    {
        $data = $this->validate([
            'routing_code' => ['required', 'numeric', 'unique:participants'],
            'participant' => ['required', 'string', 'unique:participants'],
            'classification' => ['required', 'string'],
            'notes' => ['required', 'string']
        ]);

        $createdParticipants = \App\Models\Participant::create($data);
        $this->resetInput();
        $this->successMessage = 'Participant added';
//        return redirect()->to('dashboard');
        $this->participants->prepend($createdParticipants);
    }

    public function destroy($id)
    {
        if ($id) {
            $record = \App\Models\Participant::where('id', $id);
            $record->delete();
            return redirect()->to('/dashboard');
        }
    }

    public function render()
    {
        return view('livewire.participant');
    }
}
