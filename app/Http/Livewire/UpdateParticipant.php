<?php

namespace App\Http\Livewire;

use Livewire\Component;

class UpdateParticipant extends Component
{
    public $selected_id;
    public $routing_code;
    public $participant;
    public $classification;
    public $notes;

    public $successMessage = null;
    public function resetInput()
    {
        $this->routing_code = null;
        $this->participant = null;
        $this->classification = null;
        $this->notes = null;
    }
    public function update()
    {
        $data = $this->validate([
            'routing_code' => ['required', 'numeric', 'unique:participants'],
            'participant' => ['required', 'string', 'unique:participants'],
            'classification' => ['required', 'string'],
            'notes' => ['required', 'string']
        ]);
        if ($this->routing_code) {
            $record = \App\Models\Participant::find($this->selected_id);
            $record->update([
            ]);
            $this->successMessage = 'Record updated successfully';
            $this->resetInput();
            $this->updateMode = false;
        }
    }
    public function render()
    {
        return view('livewire.update-participant');
    }
}
