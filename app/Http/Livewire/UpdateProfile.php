<?php

namespace App\Http\Livewire;

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Livewire\Component;

class UpdateProfile extends Component
{
    public $full_name;
    public $email;

    public $successMessage = null;

    public function mount()
    {
        $this->full_name = Auth::user()->name;
        $this->email = Auth::user()->email;
    }
    public function updateProfile()
    {
        $data = $this->validate([
           'full_name' => ['required','string'],
           'email' => ['email']
        ]);
        $id = Auth::user()->id;
        $user = User::find($id);
        $user->name = $this->full_name;
        $user->email = $this->email;
        $user->save();
        $this->successMessage = 'Profile Updated Successfully';
    }
    public function render()
    {
        return view('livewire.update-profile');
    }
}
